from trips.models import *
from django.contrib.auth.models import User
import datetime

mall = Category(name="mall", description="mall description")
mall.save()

airport = Category(name="airport", description="airport description")
airport.save()

railway= Category(name="railway", description="railway description")
railway.save()

restaraunt = Category(name="restaraunt", description="restaraunt description")
restaraunt.save()

forum = FamousPlace(name="forum", latx=17.385044, laty= 78.486671, cat=mall)
forum.save()
inorbit = FamousPlace(name="inorbit", latx=17.4354162,laty=78.3827308,cat=mall)
inorbit.save()
hyderabad_central = FamousPlace(name="hyderabad central", latx=17.4264, laty=78.4531, cat=mall)
hyderabad_central.save()
gvk = FamousPlace(name="forum", latx= 17.385044, laty=78.486671, cat=mall)
gvk.save()

rajiv_gandhi = FamousPlace(name="rajiv gandhi", latx=17.2400,laty=78.4281, cat=airport)
rajiv_gandhi.save()

hyderabad_deccan = FamousPlace(name="hyderabad deccan", latx=17.3924,laty= 78.4675,cat=railway)
hyderabad_deccan.save()
secunderabad= FamousPlace(name="secunderabad", latx=7.4337,laty=78.5016, cat=railway)
secunderabad= FamousPlace(name="secunderabad", latx=7.4337,laty=78.5016, cat=railway)
secunderabad.save()
kachiguda = FamousPlace(name="kachiguda", latx=17.39,laty=78.500556, cat=railway)
kachiguda.save()
vijayawada= FamousPlace(name="vijayawada",latx= 16.5182,laty=80.6185,cat=railway)
vijayawada.save()

little_italy= FamousPlace(name="little_italy",latx=17.426725, laty=78.417855,cat=restaraunt)
little_italy.save()
hardrock_cafe= FamousPlace(name="hardrock_cafe",latx=17.419572, laty=78.448514,cat=restaraunt)
hardrock_cafe.save()
n_grill= FamousPlace(name="n_grill",latx= 17.435433, laty=78.401565,cat=restaraunt)
n_grill.save()
mainland_china= FamousPlace(name="mainland_china",latx=17.431442, laty=78.456555,cat=restaraunt)
mainland_china.save()

ftod=Event(name="flight to delhi" ,description="plane departure" ,start_time=datetime.datetime(2015,12,27,2,12,5),end_time=datetime.datetime(2015,12,27,4,32,0), place=rajiv_gandhi)
ftod.save()
djsnake=Event(name="dj snake concert" ,description="plane departure" ,start_time=datetime.datetime(2015,12,13,0,0,0), end_time=datetime.datetime(2015,12,13,0,0,0), place=rajiv_gandhi)
djsnake.save()
traintov=Event(name="train to vijayawada" ,description="plane departure" ,start_time=datetime.datetime(2015,12,20,0,0,0), end_time=datetime.datetime(2015,12,21,9,9,0), place=vijayawada)
traintov.save()

u1=User.objects.create_user(first_name="aaa",last_name="aaaa",username="a123", password="password");
u1.save()
u2=User.objects.create_user(first_name="bbb",last_name="bbbb",username="b123", password="password");
u2.save()
u3=User.objects.create_user(first_name="ccc",last_name="cccc",username="c123", password="password");
u3.save()
u4=User.objects.create_user(first_name="ddd",last_name="dddd",username="d123", password="password");
u4.save()
u5=User.objects.create_user(first_name="eeee",last_name="dededd",username="e123", password="password");
u5.save()
u6=User.objects.create_user(first_name="fff",last_name="fff",username="f123", password="password");
u6.save()
u7=User.objects.create_user(first_name="gg",last_name="fgfgggf",username="g123", password="password");
u7.save()

t1=Trip(place=rajiv_gandhi,user=u1,time=datetime.datetime(2015,12,23,11,0,0));
t1.save()
t2=Trip(place=n_grill,user=u1,time=datetime.datetime(2015,12,23,18,45));
t2.save()
t3=Trip(place=secunderabad,user=u2,time=datetime.datetime(2015,12,24,9,30,0));
t3.save()
t4=Trip(place=gvk,user=u2,time=datetime.datetime(2015,12,25,12,30,0));
t4.save()
t5=Trip(place=mainland_china,user=u3,time=datetime.datetime(2015,12,20,8,15,0));
t5.save()
t6=Trip(place=forum,user=u4,time=datetime.datetime(2015,12,23,0,0,0));
t6.save()
t7=Trip(place=hyderabad_central,user=u5,time=datetime.datetime(2015,12,24,22,15,0));
t7.save()
t8=Trip(place=inorbit,user=u6,time=datetime.datetime(2015,12,24,0,15,0));
t8.save()
t9=Trip(place=little_italy,user=u6,time=datetime.datetime(2015,12,24,15,45,0));
t9.save()
t10=Trip(place=kachiguda,user=u7,time=datetime.datetime(2015,12,23,17,15,0));
t10.save()
