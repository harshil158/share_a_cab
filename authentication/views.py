from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader

from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout

from . import forms
# Create your views here.


# Create your views here.
def logoutuser(request):
    logout(request)
    return redirect("/")

def loguser(request, user=None):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect("/home")
            else:
                return redirect('/accounts/invalid')
        else:
            return redirect('/accounts/invalid')
    else:
        #return HttpResponse(forms.UserForm())
        return render_to_response("authentication/login.html",{'form': forms.UserForm()}, context_instance=RequestContext(request))
    # Return an 'invalid login' error message

def invalid(request):
    return

def register(request):
    return
