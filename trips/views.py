from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required

from models import *
import json

def index(request):
    user =  request.user.id == None

    return render_to_response("trips/index.html",
        {"login":user},
        context_instance = RequestContext(request)
    )

@login_required
def famousplaces(request):
    categories = Category.objects.all()
    data = {}
    for i in categories:
        data[i.id] = i.jsonDATA()

    return render_to_response("trips/famous.html",
        {'data':json.dumps(data)},
        context_instance = RequestContext(request)
    )

@login_required
def events(request):
    events = Event.objects.all()
    data = {}
    for i in events:
        data[i.id] = i.jsonDATA()

    return render_to_response("trips/events.html",
        {'data':json.dumps(data)},
        context_instance = RequestContext(request)
    )

@login_required
def maps(request, id=None):
    trip = Trip.objects.get(id=id)
    data = {}
    trips = Trip.objects.all()

    for i in trips:
        if i!=trip:
            data[i.id] = i.jsonDATA()

    return render_to_response("trips/map.html",
        {"data": trip.jsonDATA(), "others": json.dumps(data)},
        context_instance = RequestContext(request)
    )

@login_required
def trips(request):
    trips = Trip.objects.all()
    data = {}
    user =  request.user.id == None
    print trips
    for i in trips:
        if i.user == request.user:
            data[i.id] = i.jsonDATA()

    return render_to_response("trips/trips.html",
        {'data':json.dumps(data), "login":user},
        context_instance = RequestContext(request)
    )

@login_required
def trip(request, id=None):
    trip = Trip.objects.get(id=id)
    data = trip.jsonDATA()

    return render_to_response("trips/trip.html",
        {'data':data},
        context_instance = RequestContext(request)
    )
