"""share_a_cab URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    url(r'^$', views.index, name="/"),
    url(r'^famous/', views.famousplaces, name="famous"),
    url(r'^trips/', views.trips, name="trips"),
    url(r'^trip/(?P<id>[0-9]+)/$', views.trip, name="trip"),
    url(r'^events/', views.events, name="events"),
    url(r'^maps/(?P<id>[0-9]+)/$', views.maps, name="map"),
]
