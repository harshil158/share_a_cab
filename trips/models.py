from django.db import models
from django.contrib.auth.models import User
import json

class Place(models.Model):
    def __unicode__(self):
        return unicode(self.id) or u''

    def jsonDATA(self):
        data = {}
        data["id"] = self.id
        data["latx"] = self.latx
        data["laty"] = self.laty

        return json.dumps(data)

    latx = models.FloatField()
    laty = models.FloatField()

class Category(models.Model):
    def __unicode__(self):
        return unicode(self.name) or u''

    def jsonDATA(self):
        data = {}
        data["id"] = self.id
        data["name"] = self.name
        data["desc"] = self.description

        famousplace = {}
        for i in self.famousplace_set.iterator():
            famousplace[i.id] = i.jsonDATA()

        data["places"] = json.dumps(famousplace)
        return data

    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)

class FamousPlace(Place):
    def __unicode__(self):
        return unicode(self.name) or u''

    def jsonDATA(self):
        data = {}
        data["id"] = self.id
        data["name"] = self.name
        data["latx"] = self.latx
        data["laty"] = self.laty

        return json.dumps(data)

    name = models.CharField(max_length=30)
    cat = models.ForeignKey(Category, null=True)

class Event(models.Model):
    def __unicode__(self):
        return unicode(self.name) or u''

    def jsonDATA(self):
        data = {}
        data["latx"] = self.place.latx
        data["laty"] = self.place.laty
        data["id"] = self.id

        data["start_time"] = self.start_time
        data["last_time"] = self.end_time

        data["name"] = self.name
        data["description"] = self.description

        return json.dumps(data)

    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)

    start_time = models.TimeField()
    end_time = models.TimeField()

    place = models.ForeignKey(FamousPlace, null=True)

class Trip(models.Model):
    def __unicode__(self):
        return unicode(self.id) or u''

    def jsonDATA(self):
        data = {}
        data["lat"] = self.place.latx
        data["lng"] = self.place.laty
        data["id"] = self.id
        data["user_id"] = self.user.id
        data["time"] = str(self.time.hour) + ":" + str(self.time.minute)

        return json.dumps(data)

    place = models.ForeignKey(Place, null=True)
    user = models.ForeignKey(User, null=True)
    time = models.TimeField()
